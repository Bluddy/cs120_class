#include <stdio.h>
#include <stdbool.h>

#define CONVERT_CASE (-32)

int main(void)
{
  char lower[] = {'b', 'e', 'r', 'n'};
  char upper[4];

  upper[0] = lower[0] - 32;
  upper[1] = lower[1] - 32;
  upper[2] = lower[2] - 32;
  upper[3] = lower[3] - 32;

  putchar(upper[1]);
  putchar(upper[2]);
  putchar(upper[3]);
  putchar(upper[4]);

  return 0;
}
